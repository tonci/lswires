# README - lsWires v1.0.6 #
April 2015 drop

We are excited about Bitbucket.org and all the features it provides, public AND private, without restriction or cost.

### What is this repository for? ###

* lsWires.js - A library of functions for developing applications with Visual Studio LightSwitch.
* Version 1.0.6
* Most likely there are breaking changes from 1.0.5.  We will try and get out the updated docs as soon as we can.

### How do I get set up? ###

* Clone the project
* Enable Security Administration in the project properties
* We now package all dependencies in the lsWires.js file itself.
* Press F5 - enjoy the ride!
* On first run, a database script will prepopulate initial tiles
* We've added a folder in the project for the tile icons, you'll need to edit the tile and add as needed

### Requirements? ###

* We've changed things a little with this release in order for the library to be stable
* Edit msls-x.x.x.js.
* Search for and comment out: "use strict" (For IOS stability, not just for lsWires)
* Search for: function createScreenHashValue(screenDetails) 
* Change to reflect - if (screenDetails._dataUrl != undefined && screenDetails._dataUrl !== "" && !msls_appOptions.disableUrlScreenParameters)
* Search for: window.msls = Object.getPrototypeOf(msls) (comment it out) and add in its place: window.msls = msls
* When in doubt, do these searches on the msls-2.5.2.js in this repository and you'll see the changes
* Note that you will need to minify your msls-2.5.2.js if you don't use the one here, or if you are using 2.5.3.

### Documentation? ###

* Yes, sort of.
* http://lightswitch.codewriting.tips/
* http://blog.ofAnITGuy.com/
* We will be working on updating so please have patience

### Samples? ###

* They will be coming updated to the latest lsWires.js
* Priority... Documentation or Samples?

### Concerns? ###

This project is not the one to use with the CopySolution script due to some naming issues.  If you do, you'll notice that when you start the project in Visual Studio, the lsWires folder isn't there, but there is one with a red x.  Go ahead and "remove" that folder, then for the project, show hidden files and include the lsWires folder.  You'll be good to go.

But I would just recommend that you use the lsWireCore project, as its the recommended one to use with our copy script.  Its actually the same code, just naming changes.

